@extends('app')

@section('content')
    <div>
        @foreach($errors->all() as $message)
            <p>{{ $message }}</p>
        @endforeach
    </div>
    <div>
        @if(session()->has('message'))
            <p>{{ session()->get('message') }}</p>
        @endif
    </div>
    @if($category->exists)
        <form action="{{ route('categories.update', $category->getKey()) }}" method="POST">
        @method('PUT')
    @else
        <form action="{{ route('categories.store') }}" method="POST">
    @endif
        @csrf

        <select name="parent_id">
            <option value="">Select parent category</option>
            @foreach($categories as $selectionCategory)
                <option @if($category->parent?->getKey() == $selectionCategory->getKey()) selected @endif 
                    value="{{ $selectionCategory->getKey() }}">{{ $selectionCategory->getName() }}</option>
            @endforeach
        </select>
        <input type="text" name="name" value="{{ old('name', $category->exists ? $category->getName() : '') }}">
        <input type="submit" value="Send">
    </form>
@endsection