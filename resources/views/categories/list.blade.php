@extends('app')

@section('content')
    <ul>
       @foreach($categories as $category)
            <li>
                <a href="{{ route('categories.show', $category->getKey()) }}">{{ $category->getName() }}</a>
            </li>
       @endforeach
    </ul>
@endsection