<?php

namespace App\Http\Controllers;

use App\Exceptions\BusinessException;
use App\Http\Requests\SaveCategoryRequest;
use App\Models\Category;
use Illuminate\Support\Facades\View;

class CategoryConroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();

        return View::make('categories.list', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Category $category)
    {
        $categories = Category::all();

        return View::make('categories.form', compact('category', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  SaveCategoryRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SaveCategoryRequest $request)
    {
        Category::create($request->validated());
        
        return back()->with('message', 'Created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        return View::make('categories.show', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $categories = Category::all();

        return View::make('categories.form', compact('category', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  SaveCategoryRequest  $request
     * @param  Category $category
     * @return \Illuminate\Http\Response
     */
    public function update(SaveCategoryRequest $request, Category $category)
    {   
        try {
            $parent = Category::find($request->parent_id);
            $category->setName($request->name);
            $category->attachParent($parent);
            $category->save();
            return back()->with('message', 'Updated.');
        } catch(BusinessException $e) {
            return back()->withInput()->withErrors(['message' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        if ($category->hasChildren()) {
            return back()->withErrors('message', 'The category has children.');       
        }

        $category->delete();
        return back()->with('message', 'Deleted.');
    }
}
