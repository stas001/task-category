<?php

namespace App\Models;

use App\Exceptions\BusinessException;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Category extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'parent_id'];

    /**
     * @return BelongsTo
     */
    public function parent(): BelongsTo
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }
    
    /**
     * @param Category|null $category
     */
    public function attachParent(Category|null $category): void
    {
        if (is_null($category)) {
            $this->setParentId($category);
        }
        $child = $this->children?->where('id', $category->getKey() )->first();
        
        if ($child) {
            throw new BusinessException('The category is a child.');
        }

        $this->setParentId($category->getKey());
    }

    /**
     * @return HasMany
     */
    public function children(): HasMany
    {
        return $this->hasMany(Category::class, 'parent_id', 'id');
    }

    /**
     * @param bool
     */
    public function hasChildren(): bool
    {
        return (bool) $this->children->count();
    }

    /**
     * @return null|string
     */
    public function getName(): ?string 
    {
        return $this->name;
    }

    /**
     * @param string
     */
    public function setName(string $name): void 
    {
        $this->name = $name;
    }

    /**
     * @param null|int
     */
    public function setParentId(int|null $parentId): void 
    {
        $this->parent_id = $parentId ? $parentId : 0;
    }
}   
